package edu.hawaii.ics211;

/**
 * Test the Calculator methods
 * 
 * @author michellelau
 */
public class TestCalculator {

	/**
	 * The main method to instantiate objects
	 * @param args An array of Strings
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calculator calculate = new Calculator();

		System.out.println(calculate.add(4, 2));
		System.out.println(calculate.subtract(2, 1));
		System.out.println(calculate.multiply(3, 1));
		System.out.println(calculate.divide(4, 2));
		System.out.println(calculate.modulo(3, 3));
		System.out.println(calculate.pow(2, 3));
		
	}

}
